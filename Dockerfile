FROM ubuntu:18.04

# install pandoc
RUN apt update && \
        apt install -yqq pandoc && \
        rm -rf /var/lib/apt/lists

WORKDIR /workspace

# set command
CMD pandoc
