# Pandoc Docker image

## Build image

```
docker build -t csabasulyok/pandoc .
```

## Run pandoc through image

```
docker run -it --rm -v "$(pwd)":/workspace csabasulyok/pandoc pandoc
```

## Alias pandoc for bash

```
alias pandoc="docker run -it --rm -v \"$(pwd)\":/workspace csabasulyok/pandoc pandoc"
```

## Set alias generally in bash

```
echo 'alias pandoc=\"docker run -it --rm -v \\\"$(pwd)\\\":/workspace csabasulyok/pandoc pandoc\"' >> ~/.bashrc
```